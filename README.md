Prerequisite
===

1. drool workbench
2. wildfly
3. java 8
4. jmeter


Set-up
===

1. Download wildfly 8
2. Decompress it
3. Go to bin folder 
4. Create a management user and application user using ./add-user.sh
    a. Management User: Any username and password will do and leave the groups as blank
    b. Application User: Any username and password will do and input in the groups "analyst,admin"
5. Run ./standalone.sh
6. Download [drools workbench war file](http://download.jboss.org/drools/release/6.3.0.Final/kie-drools-wb-6.3.0.Final-wildfly8.war)
7. Go to [wildfly manager](http://localhost:9990/console/App.html) and login your management user
8. Go to deployment and upload the war file
9. Go drools-csapp folder then go to facts folder and compile it using mvn clean install
10. Log in your application user [here](http://localhost:8080/kie-drools-wb-6.3.0.Final-wildfly8) 
11. Go to artifact repository, Upload the compiled facts (usually located in ./m2/repository/<package name>)
12. Create organizational unit and repository for it in authoring > administration
13. Create project in the newly made organizational unit and its corresponding repository
14. Create drool file and the package declaration must the same as the compiled facts
15. Validate it
16. Click Open project editor and add the uploaded facts as dependency
17. Click build then choose build and deploy
18. Find the RulesConfig.java in the course suggestion app and change the URL to the download URL
<workbench URL> + /maven2/ + value of PATH in Artifact repository
19. Run the the course suggestion app using mvn spring-boot:run
20. Download jmeter 
21. Unzip it
22. In the jmeter, open the drools.jmx file. Edit the parameters and enable the 'admin/rules/add' 
if you are going to download the drools file.
 
