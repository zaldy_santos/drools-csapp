package com.droolscsapp.data.repository;

import com.droolscsapp.data.domain.RuleArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author zsantos
 */
public interface RuleArtifactRepository extends PagingAndSortingRepository<RuleArtifact, Long>{
    
    RuleArtifact findByActiveTrue();
}
