package com.droolscsapp.service;

import com.droolscsapp.api.resource.SuggestionResource;
import com.droolscsapp.fact.Suggestions;
import com.droolscsapp.fact.SubjectRating;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
/**
 *
 * @author zsantos
 */
@Service
public class CourseService {
    
    @Autowired
    private KieContainer kieContainer;

    public SuggestionResource suggestCourses(Map<String, Integer> subjectRating) {
        StatelessKieSession courseMatchSession = kieContainer.newStatelessKieSession();
        SuggestionResource response = new SuggestionResource();
        List<Object> facts = subjectRating.entrySet()
                                          .stream()
                                          .map(stringStringEntry -> new SubjectRating(stringStringEntry.getKey(),
                                                                                      stringStringEntry.getValue()))
                                          .collect(toList());

        Suggestions suggestions = new Suggestions();
        courseMatchSession.setGlobal("suggestions", suggestions);
        courseMatchSession.execute(facts);
  
        suggestions.getSuggestedCourseCodes()
                   .forEach(response::addSuggestion);

        return response;
    }
}
