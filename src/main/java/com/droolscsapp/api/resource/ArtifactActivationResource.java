package com.droolscsapp.api.resource;

/**
 *
 * @author zsantos
 */
public class ArtifactActivationResource {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
