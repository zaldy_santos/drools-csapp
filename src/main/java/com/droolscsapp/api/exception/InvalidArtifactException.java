package com.droolscsapp.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 *
 * @author zsantos
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND,
                reason = "Artifact does not exist.")
public class InvalidArtifactException extends RuntimeException{
    
}
