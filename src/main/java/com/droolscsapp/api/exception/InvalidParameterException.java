package com.droolscsapp.api.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 *
 * @author zsantos
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST,
                reason = "There was a problem parsing the parameters provided. Please review the API Documentation.")
public class InvalidParameterException extends RuntimeException{
    
}
