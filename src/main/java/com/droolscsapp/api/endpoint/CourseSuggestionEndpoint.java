package com.droolscsapp.api.endpoint;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.droolscsapp.api.exception.InvalidParameterException;
import com.droolscsapp.service.CourseService;
import com.droolscsapp.api.resource.SuggestionResource;

/**
 *
 * @author zsantos
 */
@RestController
@RequestMapping(value = "/course")
public class CourseSuggestionEndpoint {
    
    @Autowired
    private CourseService courseService;
    
    @RequestMapping(value = "/suggestion",method = RequestMethod.POST)
    public SuggestionResource SuggestCourse(@RequestBody Map<String, Integer> request) throws JsonProcessingException{ 
        try {
            return courseService.suggestCourses(request);
        } catch (IllegalArgumentException e) {
            throw new InvalidParameterException();
        }
        
    }
}
