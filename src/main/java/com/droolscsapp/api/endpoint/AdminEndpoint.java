package com.droolscsapp.api.endpoint;

import com.droolscsapp.api.resource.ArtifactActivationResource;
import com.droolscsapp.api.resource.RulesArtifactResource;
import com.droolscsapp.service.RuleAdminService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author zsantos
 */
@RestController
@RequestMapping(value = "/admin")
public class AdminEndpoint {
    
    @Autowired
    private RuleAdminService ruleAdminService;

    @RequestMapping(value = "/rules/activate",
                    method = RequestMethod.POST)
    public void activateRuleArtifact(
            @RequestBody ArtifactActivationResource artifactActivationResource) throws IOException {
        ruleAdminService.activateRuleArtifact(artifactActivationResource);
    }

    @RequestMapping(value = "/rules/add",
                    method = RequestMethod.POST)
    public void addRuleArtifact(@RequestBody RulesArtifactResource ruleArtifactResource) throws IOException {
        ruleAdminService.addRuleArtifact(ruleArtifactResource);
    }
}
